# A Helm chart for wallet-broker
This chart is used to demonstrate the Monsterpay application and display
features of an Istio service mesh framework.

In particular, we demonstrate how different versions of wallet broker can
be activated in a cluster by using Istio Route Rules.

For details, see
[Blue/Green Deployment using Istio Route Rules](https://bitbucket.org/objectcomputing/cloud-native-blueprint/src/master/istio-route-rules.md)

## Blue/Green Routing (gist - See above link for details)
### Use Helm upgrade to _include_ the new version of wallet-broker
set flag `rollingUpdate.enabled=true`

See the [manifest](./wallet-broker/templates/deployment.yaml) and the chart
[values](./wallet-broker/values.yaml) file for how to get both version running.

If changes are local and Helm is executed locally in this directory (`chart-wallet-broker`),
replace the last line with `./wallet-broker`

Both versions of wallet-broker must be running:

```bash
    $ helm upgrade wallet-broker-v1 \
            --namespace=app  \
            --set service.redisLink=redis-redis  \
            --set issuerRelease=wallet-issuer-v1 \
            --set rollingUpdate.enabled=true \
            $STABLE_REPO_NAME/wallet-broker
```

where `$STABLE_REPO_NAME` is an env var pointing to the Helm stable repository
containing this chart.  In realty, this feature would probably be done on a
development branch (`incubator`) and so, when built/deployed by Jenkins, we
would point to something like `INCUBATOR_REPO_NAME`, rather than stable.

#### Verify that *both* `master` and `serial` wallet-broker pods are running:

```bash
    $ kubectl get pods --namespace app

    NAME                                                   READY     STATUS    RESTARTS   AGE
    redis-redis-bfff6d675-hb2pt                            2/2       Running   0          2d
    wallet-broker-v1-wallet-broker-5cc45f5d98-dtp6h        2/2       Running   0          1h
    wallet-broker-v1-wallet-broker-serial-544569cd-v48mg   2/2       Running   0          1h
    wallet-issuer-v1-issuer1-5f64c9bf7d-7vgxv              2/2       Running   0          2d
    wallet-issuer-v1-issuer2-7ff7f78855-jrlbn              2/2       Running   0          2d
    wallet-issuer-v1-issuer3-59d5dc4856-2rz54              2/2       Running   0          2d
    wallet-ui-v1-wallet-ui-895c7d978-vrkwm                 2/2       Running   0          1d
```

### Create default (`parallel`) Istio route rule
(see [file](./route-rule-default.yaml) in this, `chart-wallet-broker`, directory):

```bash
    $ istioctl create -f route-rule-default.yaml
```

#### Verify it's running:

```bash
    $ istioctl get routerule wallet-broker -o yaml -n app

    apiVersion: config.istio.io/v1alpha2
    kind: RouteRule
    metadata:
      creationTimestamp: null
      name: wallet-broker
      namespace: app
      resourceVersion: "319860"
    spec:
      destination:
        name: wallet-broker-v1-wallet-broker
        namespace: app
      precedence: 1
      route:
      - labels:
          version: master
        weight: 100
    ---
```

* Make a request with the wallet-ui using `lisa@gmail.com` and any password.
* When we view the call trace in the Jaeger distributed tracing UI, we notice that
we're invoking the `parallel` version of wallet-broker because the calls to the
issuer service are stacked.

#### If Jaeger is not running in this cluster, we can verify that the wallet-ui
by looking at the logs for wallet-broker `parallel` pod:


```bash
    $ kubectl logs wallet-broker-v1-wallet-broker-5cc45f5d98-dtp6h -c wallet-broker -n app`
```

### Now route calls to the `serial` version:
(see [file](./route-rule-serial.yaml) in this, `chart-wallet-broker`, directory):

```bash
    $ istioctl replace -f route-rule-serial.yaml

    Updated config route-rule/app/wallet-broker to revision 320041

    $ istioctl get routerule wallet-broker -n app -o yaml

    apiVersion: config.istio.io/v1alpha2
    kind: RouteRule
    metadata:
      creationTimestamp: null
      name: wallet-broker
      namespace: app
      resourceVersion: "320041"
    spec:
      destination:
        name: wallet-broker-v1-wallet-broker
        namespace: app
      precedence: 1
      route:
      - labels:
          version: serial
        weight: 100
    ---
```

* Make a request with the wallet-ui using `janedoe@gmail.com` and any password.
* When we view the call trace in Jaeger, we notice that we're now invoking the
`serial`version of wallet-broker because the calls to the issuer service are
spread out.

#### If Jaeger is not running in this cluster, we can verify that the wallet-ui
by looking at the logs for wallet-broker `serial` pod:

```bash
    $ kubectl logs wallet-broker-v1-wallet-broker-serial-544569cd-v48mg -c wallet-broker -n app
```

